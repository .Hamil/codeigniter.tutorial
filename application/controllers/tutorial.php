<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class tutorial extends CI_Controller {//Hacemos herencia del controlador padre para que se carguen los metodos que están en dicho controlador
    function __construct(){
        parent::__construct();
        $this->load->helper('mihelper');
        $this->load->helper('form_helper');
        $this->load->model('tutorial_model');
    }
    function index(){
        $this->load->library('Menu',array("Inicio","Contacto","Cursos"));
        $data['menu'] = $this->menu->construirMenu();
        $this->load->view('tutorial/bienvenido', $data);
    }

}

?>