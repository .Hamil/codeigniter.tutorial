<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class cursos extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper('mihelper');
        $this->load->helper('form_helper');
        $this->load->model('tutorial_model');
    }

    function index(){
        $data['cursos'] = $this->tutorial_model->obtenerCursos();
        $this->load->view('tutorial/headers');
        $this->load->view('cursos/cursos', $data);
    }

    function nuevo(){
        $this->load->view('tutorial/headers');
        $this->load->view('cursos/formulario');
    }

    function recibirDatos(){
        $data = array(
            "nombre" => $this->input->post("nombre"),
            "videos" => $this->input->post("videos")
        );
        $this->tutorial_model->crearCurso($data);
        $this->load->view('tutorial/headers');
        $this->load->view('tutorial/bienvenido');
    }
}
?>