<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Menu{
        private $arr_menu;
        public function __construct($array){
            $this->arr_menu = $array;
        }
        
        public function construirMenu(){
            $ret_menu = "<nav><ul>";
            foreach($this->arr_menu as $menu){
                $ret_menu .= "<li>".$menu."</li>";
            }
            $ret_menu .= "</ul></nav>";
            return $ret_menu;
        }
    }
?>