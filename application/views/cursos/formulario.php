<?= form_open('cursos/recibirdatos') ?>

<?php
    $nombre = array(
        "name" => "nombre",
        "placeholder" => "Escribe tu nombre"
    );

    $videos = array(
        "name" => "videos",
        "placeholder" => "Cantidad de videos del curso"
    );
?>
<?= form_label('Nombre: ','nombre') ?>
<?= form_input($nombre) ?>
<br><br><br>
<label for="">
    <?= form_label('Número de vídeos','videos') ?>
    <?= form_input($videos) ?>
</label>
<?= form_submit('','Subir curso') ?>
<?= form_close() ?>
</body>
</html>